﻿using NetXP.NetStandard.Processes;
using NetXP.NetStandard.Processes.Implementations;
using System;
using System.Linq;
using System.Threading.Tasks;
using Unosquare.PiGpio.NativeEnums;
using Unosquare.PiGpio.NativeMethods;

namespace WaitForExitAfterSocketTimeout
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 5; i++)//Five zombie process in linux
            {

                Console.WriteLine($"Write GPIO Test {i}");
                try
                {
                    var gepioInitializeResult = Setup.GpioInitialise();
                    if (gepioInitializeResult < 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        throw new Exception($"Fail GpioInitialise: {gepioInitializeResult.ToString()}");
                    }

                    var pin = (SystemGpio)20;
                    IO.GpioSetMode(pin, PinMode.Output);

                    IO.GpioWrite(pin, i % 2 == 0);

                    Console.WriteLine(i % 2 == 0 ? "ON" : "OFF");
                }
                catch (System.Exception nse)
                {
                    Console.WriteLine(nse.ToString());
                }
                finally
                {
                    Setup.GpioTerminate();
                }

                // Act
                IOTerminal iOTerminal = new IOTerminal();
                var result = iOTerminal.Execute(new ProcessInput
                {
                    Command = "cat /proc/cpuinfo | grep Serial | awk ' {print $3}'",
                    ShellName = "/bin/bash",
                    Arguments = ""
                });

                result.StandardOutput =
                    result.StandardOutput
                    .Where(o => o?.Trim()?.Equals("") != true).ToArray(); ;

                Console.WriteLine($"Process {i} => { string.Join(",", result.StandardOutput)}");

                Task.Delay(5000).Wait();
            }

            Console.Write("Press any key to exit.");
            Console.ReadKey();

        }
    }
}
